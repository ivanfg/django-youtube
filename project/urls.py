"""Project resource configuration
"""
from django.contrib import admin
from django.urls import include, path

from youtube import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('/<str:id>', include('youtube.urls')),
    path('', include('youtube.urls')),
]
